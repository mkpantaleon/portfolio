# README #

This repo was created for the INFO2300 bitbucket startup assignment.

### What is this repository for? ###

* The contents lists in this repo showcase a portfolio I created in a previous class and can be altered for personal use.

### Deployment Instructions ###

* Open Visual Studio Code.
* From the side bar menu, click on 'clone repository' and paste the repo url below to copy files locally.
* https://mkpantaleon@bitbucket.org/mkpantaleon/portfolio.git 
* Launch 'index.html' file to view website portfolio.

### Who do I talk to? ###

* For Support / Inquiries: michael.pantaleon5818@conestogac.on.ca

### License ###

* This repo utilizes an MIT license with conditions only requiring preservation of copyright and license notices. It allows for commercial use, distribution, modification and private use. Those who access this repo
have permissions to replicate, duplicate and alter.
