### Project Wiki ###

* [Main Repo](https://bitbucket.org/mkpantaleon/portfolio/src/master/)
* [License.md](https://bitbucket.org/mkpantaleon/portfolio/src/master/LICENSE.md)
* [Readme.md](https://bitbucket.org/mkpantaleon/portfolio/src/master/README.md)

### Article ###

* The purpose of private repositories is to save your code without having it in the open. Such as work in progress projects (CAPSTONE) that are proprietary at the moment and currently doesn't need to be shared.
Effectively private repo's are just a place to back-up your private code in a remote repository. It doesn't make sense to make it public for the world to see unless they are part of the project, especially if its being made for a specific client.

### Article Pt 2 ### 

* Reasons why Signal PM is open source
* accelerate the dev of typical product since devs from all over the world are contributing
* open the ability to solve a problem that many devs are facing
* get recognition - most devs define their identity of projects they are a part of
* access to free sources for everybody 